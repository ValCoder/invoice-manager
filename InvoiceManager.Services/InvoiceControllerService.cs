﻿namespace InvoiceManager.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Models.BindingModels.Invoice;
    using Models.BindingModels.Recipient;
    using Models.EntityModels;
    using Models.EntityModels.Invoice;
    using Models.ViewModels.Invoice;

    public class InvoiceControllerService : Service
    {
        
        #region Check if its first time for current user to create invoice

        public bool IsFirstTimeToCreateInvoice(string currentUserId)
        {
            ApplicationUser currentUser = this.GetCurrentUserById(currentUserId);

            if (currentUser != null)
            {
                if (CurrentUserHaveBussinessInfoInInvoice(currentUser))
                {
                    return true;
                }
            }
            return false;
        }

        #endregion


        #region Add bussines credentials to current user

        public void AddBussinessCredentialsToCurrentUser(string currentUserId, BussinessCredentialsBindingModel bcbm)
        {
            if (bcbm.ProviderName != null && bcbm.Address != null && bcbm.AccountablePerson != null)
            {
                BussinessCredentials bussinesCredentialEntity =
                    Mapper.Map<BussinessCredentialsBindingModel, BussinessCredentials>(bcbm);

                ApplicationUser currentUser = this.GetCurrentUserById(currentUserId);

                if (currentUser != null)
                {
                    currentUser.BussinessCredentials = bussinesCredentialEntity;
                    this.UnitOfWork.Commit();
                }
            }
        }

        #endregion


        #region Get company credentials for current user

        public CompanyCredentialDataViewModel GetBussinesCredentialForCurrentUser(string currentUserId)
        {
            ApplicationUser currentUser = this.GetCurrentUserById(currentUserId);

            if (currentUser != null)
            {
                var viewModel = new CompanyCredentialDataViewModel()
                {
                    Address = currentUser.BussinessCredentials.Address,
                    ProviderName = currentUser.BussinessCredentials.ProviderName,
                    AccountablePerson = currentUser.BussinessCredentials.AccountablePerson,
                    Uic = currentUser.BussinessCredentials.Uic
                };

                return viewModel;
            }
            return null;
        }


        #endregion


        #region Add invoice info to current user

        public void AddInvoiceInfo(string currentUserId, AddInvoiceInfoBindingModel aiibm)
        {
            ApplicationUser currentUser = this.GetCurrentUserById(currentUserId);

            var userHasBussinessCredentials = currentUser?.BussinessCredentials != null;

            if (userHasBussinessCredentials)
            {
               // Invoice invoiceEntity = Mapper.Map<AddInvoiceInfoBindingModel, Invoice>(aiibm);
                Invoice invoiceEntity = new Invoice()
                {
                    NumberOfInvoice = aiibm.NumberOfInvoice,
                    PhoneNumber = aiibm.PhoneNumber,
                    Fax = aiibm.Fax,
                    Address = currentUser.BussinessCredentials.Address,
                    Email = aiibm.Email,
                    BussinessLogoUrl = aiibm.BussinessLogoUrl,
                    BussinessDomainAddress = aiibm.BussinessDomainAddress,
                    BussinessCredentials = currentUser.BussinessCredentials,
                    Date = DateTime.Now,
                    UserId = currentUser.Id,

                };
                currentUser.Invoices.Add(invoiceEntity);
                this.UnitOfWork.Commit();
            }
        }

        #endregion


        #region Add invoice value to current user

        public bool AddInvoiceValue(string currentUserId, InvoiceValueBindigModel ivbm)
        {
            ApplicationUser currentUser = this.GetCurrentUserById(currentUserId);

            if (currentUser != null)
            {
                // check created invoice to have recipient, if not delete the last invoice

                int lastCurrentUserInvoiceId = currentUser.Invoices.Max(invoice => invoice.Id);
                var invoiceToCheckForRecipeint = this.UnitOfWork.Invoices.Find(lastCurrentUserInvoiceId);
                bool hasRecepient = false;

                if (invoiceToCheckForRecipeint != null)
                {
                    hasRecepient = invoiceToCheckForRecipeint.Recipient != null;
                }

                if (hasRecepient)
                {
                    InvoiceValue invoiceValue = new InvoiceValue()
                    {
                        TypeOfService = ivbm.TypeOfService,
                        SinglePrice = ivbm.SinglePrice,
                        Quantity = ivbm.Quantity,
                        DateOfDelivery = ivbm.DateOfDelivery,
                        PaymentAmount = ivbm.PaymentAmount,
                        ResultValue = ivbm.ResultValue,
                        TaxBase = ivbm.TaxBase,
                        TaxRatePercentage = ivbm.TaxRatePercentage,
                        TypeOfMeasure = ivbm.TypeOfMeasure,
                        // MethodOfPayment = ivbm.MethodOfPayment,
                        VatRate = ivbm.VatRate,

                    };

                    int lastInvoiceId = currentUser.Invoices.Max(invoice => invoice.Id);
                    var curentUserInvoice =
                        currentUser.Invoices.FirstOrDefault(
                            invoice => invoice.UserId == currentUser.Id && invoice.Id == lastInvoiceId);

                    if (CheckIfValueExists(currentUser, ivbm))
                    {
                        curentUserInvoice?.InvoiceValues.Add(invoiceValue);
                        this.UnitOfWork.Commit();

                        return true;
                    }
                }

                var lastInvoiceById = currentUser.Invoices.Max(invoice => invoice.Id);
                var invoiceToRemove = this.UnitOfWork.Invoices.Find(lastInvoiceById);

                if (invoiceToRemove != null)
                {
                    this.UnitOfWork.Invoices.Remove(invoiceToRemove);
                    this.UnitOfWork.Commit();
                    return false;
                }

            }
            return false;
        }

        #endregion


        #region Ckeck if invoice value exists

        private bool CheckIfValueExists(ApplicationUser currentUser, InvoiceValueBindigModel ivbm)
        {
            var currentUserInvoice = currentUser.Invoices.FirstOrDefault(invoice => invoice.UserId == currentUser.Id);

            // create new invoice value when invoice value entity is empty in db
            if (currentUserInvoice != null && !currentUserInvoice.InvoiceValues.Any())
            {
                return true;
            }

            // check invoice value not to be duplicated in db
            var exists = currentUserInvoice != null && currentUserInvoice.InvoiceValues.
                             Any(i => i.TypeOfService == ivbm.TypeOfService &&
                                      i.Quantity == ivbm.Quantity &&
                                      i.SinglePrice == ivbm.SinglePrice);

            if (exists)
            {
                return false;
            }
            return true;
        }


        #endregion


        #region Check if current user have bussiness credentials in db

        private bool CurrentUserHaveBussinessInfoInInvoice(ApplicationUser currentUser)
        {
            if (
                currentUser.Invoices.Any(
                    invoice =>
                        invoice.BussinessCredentials.Address != null &&
                        invoice.BussinessCredentials.AccountablePerson != null
                        && invoice.BussinessCredentials.ProviderName != null &&
                        invoice.BussinessCredentials.TypeOfManagment != null
                        && invoice.BussinessCredentials.Uic != null && currentUser.Invoices.Any()))
            {
                return true;
            }
            return false;
        }

        #endregion


        #region Show all invoices for current users and call delete unfinished invoice method

        public IEnumerable<AllInvoicesViewModel> GetAllInvoicesForCurrentUser(string currentUserId)
        {
            DeleteUnfinishedInvoice(currentUserId);
            List<AllInvoicesViewModel> viewModels = new List<AllInvoicesViewModel>();

            ApplicationUser currentUser = this.GetCurrentUserById(currentUserId);

            if (currentUser != null)
            {
                var currentUserInvoices = currentUser.Invoices.ToList();
                var currentUserBussinesCredentials = currentUser.BussinessCredentials;
                var currentUserInvoicesCount = currentUserInvoices.Count;

                foreach (var userInvoice in currentUserInvoices)
                {

                    var dateofDelivary = new DateTime();
                    string typeOfService = string.Empty;

                    foreach (var invoiceValue in userInvoice.InvoiceValues)
                    {
                        dateofDelivary = invoiceValue.DateOfDelivery;
                        typeOfService = invoiceValue.TypeOfService;
                    }

                    var viewModel = new AllInvoicesViewModel()
                    {
                        Id = userInvoice.Id,
                        TypeOfService = typeOfService,
                        NumberOfInvoice = userInvoice.NumberOfInvoice,
                        ProviderName = currentUserBussinesCredentials.ProviderName,
                        DateOfdDelivery = dateofDelivary,
                        RecepientName = userInvoice.Recipient?.RecipientName,
                        NumberOfAllUserInvoices = currentUserInvoicesCount
                    };

                    viewModels.Add(viewModel);
                }
            }
            return viewModels;
        }

        #endregion


        #region Add Recepient 

        public void AddRecepient(RecipientBindingModel rbm, string currentUserId)
        {

            ApplicationUser currentUser = this.GetCurrentUserById(currentUserId);

            if (currentUser != null)
            {

                // get the last invoice with no recipient

                int lastInvoiceId = currentUser.Invoices.Max(invoice => invoice.Id);

                var currentInvoice =
                    currentUser.Invoices.FirstOrDefault(
                        invoice =>
                            invoice.UserId == currentUser.Id && invoice.Id == lastInvoiceId &&
                            invoice.Recipient == null);

                if (currentInvoice != null)
                {
                    currentInvoice.Recipient = new Recipient()
                    {
                        RecipientName = rbm.RecipientName,
                        Address = rbm.Address,
                        Uic = rbm.Uic,
                        AccountablePerson = rbm.AccountablePerson
                    };
                    this.UnitOfWork.Commit();
                }
            }
        }

        #endregion


        #region Create new invoice for current user

        public void CreateNewInvoiceForCurrentUser(string currentUserId)
        {
            ApplicationUser currentUser = this.GetCurrentUserById(currentUserId);

            if (currentUser != null)
            {
                // get current user max invoice id (last invoice in table)
                int currentUserLastInvoiceId = currentUser.Invoices.Max(invoice => invoice.Id);

                // find last invoice by id 
                var currentUserLastInvoice = currentUser
                    .Invoices.FirstOrDefault(invoice => invoice.UserId == currentUser.Id &&
                                                        invoice.Id == currentUserLastInvoiceId);

                // create new invoice with different numberOfInvoice and Date
                if (currentUserLastInvoice != null)
                {
                    var invoiceEntity = new Invoice()
                    {
                        Address = currentUserLastInvoice.Address,
                        BussinessCredentials = currentUserLastInvoice.BussinessCredentials,
                        PhoneNumber = currentUserLastInvoice.PhoneNumber,
                        BussinessDomainAddress = currentUserLastInvoice.BussinessDomainAddress,
                        BussinessLogoUrl = currentUserLastInvoice.BussinessLogoUrl,
                        Date = DateTime.Now,
                        Email = currentUserLastInvoice.Email,
                        Fax = currentUserLastInvoice.Fax,
                        NumberOfInvoice = currentUserLastInvoice.NumberOfInvoice + 1,
                        UserId = currentUser.Id
                    };
                    currentUser.Invoices.Add(invoiceEntity);
                    this.UnitOfWork.Commit();
                }
            }
        }

        #endregion


        #region Get ready invoce view model

        public ReadyInvoiceViewModel GetReadyInvoice(string currentUserId, int? invoiceId)
        {
            var viewModel = this.GetReadyInvoiceToPdf(currentUserId, invoiceId);

            return viewModel;
        }

        #endregion


        #region Delete invoice view model GET
        public DeleteInvoiceViewModel GetInvoiceByIdToDelete(string currentUserId, int? invoiceId)
        {
            ApplicationUser currentUser = this.GetCurrentUserById(currentUserId);

            var invoice = currentUser?.Invoices.FirstOrDefault(i => i.UserId == currentUserId && i.Id == invoiceId);

            if (invoice != null)
            {
                DeleteInvoiceViewModel viewModel = new DeleteInvoiceViewModel()
                {
                    InvoiceValues = invoice.InvoiceValues,
                    NumberOfInvoice = invoice.Id,
                    ProviderName = invoice.BussinessCredentials.ProviderName,
                    RecepientName = invoice.Recipient.RecipientName,
                    InvoiceId = invoice.Id
                };
                return viewModel;
            }
            return null;
        }

        #endregion


        #region Delete invoice POST

        public void DeleteCurrentUserInvoiceById(int invoiceId, string userId)
        {
            var currentUser = this.GetCurrentUserById(userId);

            var incoice =
                currentUser?.Invoices.FirstOrDefault(invoice => invoice.UserId == userId && invoice.Id == invoiceId);

            if (incoice != null)
            {
                var invoiceValues = incoice.InvoiceValues;

                this.UnitOfWork.InvoiceValues.RemoveRange(invoiceValues.ToList());

                this.UnitOfWork.Invoices.Remove(incoice);
                this.UnitOfWork.Commit();
            }
        }

        #endregion


        #region Delete unfinished invoice

        private void DeleteUnfinishedInvoice(string currentUserId)
        {
            var currentuser = this.GetCurrentUserById(currentUserId);

            // if any invoice recepient is null this invoice is unfinished => delete it

            var invoiceWithoutRecepient = currentuser?.Invoices.Where(invoice => invoice.Recipient == null);
            

            if (invoiceWithoutRecepient != null)
            {
                this.UnitOfWork.Invoices.RemoveRange(invoiceWithoutRecepient.ToList());
                this.UnitOfWork.Commit();
            }
        }

        #endregion


        #region Get user by id

        private ApplicationUser GetCurrentUserById(string currentUserId)
        {
            return this.UnitOfWork.Users.FirstOrDefaultEntity(user => user.Id == currentUserId);
        }

        #endregion


        #region Return invoice ready view for pdf(ReadyInvoiceView)
        private ReadyInvoiceViewModel GetReadyInvoiceToPdf(string currentUserId, int? invoiceId)
        {
            ReadyInvoiceViewModel viewModel;

            var currentUser = this.GetCurrentUserById(currentUserId);

            // get the invoice pdf view by given id
            if (invoiceId != null && currentUser != null)
            {
                var invoiceToPdf = currentUser.Invoices.FirstOrDefault(invoice => invoice.Id == invoiceId);

                if (invoiceToPdf != null)
                {
                    decimal vatRatePercentage = 0;
                    decimal vatRatesSum = 0;

                    foreach (var invoiceValue in invoiceToPdf.InvoiceValues)
                    {
                        vatRatePercentage = invoiceValue.TaxRatePercentage;
                        vatRatesSum += invoiceValue.VatRate;
                    }

                    viewModel = new ReadyInvoiceViewModel()
                    {
                        Invoice = invoiceToPdf,
                        BussinessCredentials = invoiceToPdf.BussinessCredentials,
                        Recipient = invoiceToPdf?.Recipient,
                        AllResultValuesSum = invoiceToPdf.InvoiceValues.Sum(iv => iv.ResultValue),
                        PaymentResultSum = invoiceToPdf.InvoiceValues.Sum(iv => iv.PaymentAmount),
                        VatRatePecentage = vatRatePercentage,
                        VatRatesSum = vatRatesSum,
                        InvoiceId = invoiceToPdf.Id
                    };
                    return viewModel;
                }
            }

            // get the last invoice of current user and set it to pdf view
            if (currentUser != null)
            {
                var lastCurrentUserInvoiceId = currentUser.Invoices.Max(invoice => invoice.Id);
                var lastCurrentUserInvoice = this.UnitOfWork.Invoices.Find(lastCurrentUserInvoiceId);

                if (lastCurrentUserInvoice != null)
                {
                    decimal vatRatePercentage = 0;
                    decimal vatRatesSum = 0;

                    foreach (var invoiceValue in lastCurrentUserInvoice.InvoiceValues)
                    {
                        vatRatePercentage = invoiceValue.TaxRatePercentage;
                        vatRatesSum += invoiceValue.VatRate;
                    }

                    viewModel = new ReadyInvoiceViewModel()
                    {
                        Invoice = lastCurrentUserInvoice,
                        BussinessCredentials = lastCurrentUserInvoice.BussinessCredentials,
                        Recipient = lastCurrentUserInvoice?.Recipient,
                        AllResultValuesSum = lastCurrentUserInvoice.InvoiceValues.Sum(iv => iv.ResultValue),
                        PaymentResultSum = lastCurrentUserInvoice.InvoiceValues.Sum(iv => iv.PaymentAmount),
                        VatRatePecentage = vatRatePercentage,
                        VatRatesSum = vatRatesSum,
                        InvoiceId = lastCurrentUserInvoiceId
                    };
                    return viewModel;
                }

            }
            return null;
        }
        #endregion
    }
}