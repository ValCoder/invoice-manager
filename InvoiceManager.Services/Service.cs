﻿namespace InvoiceManager.Services
{
    using Data;
    public abstract class Service
    {
        protected Service()
        {
           // this.Context = new InvoiceManagerContext();
           this.UnitOfWork = new UnitOfWork();
            

        }

     //   protected InvoiceManagerContext Context { get; }

        protected UnitOfWork UnitOfWork { get; set; }
    }
}
