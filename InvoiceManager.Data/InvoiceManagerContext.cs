using System.Data.Entity;
using InvoiceManager.Models.EntityModels;
using InvoiceManager.Models.EntityModels.Invoice;
using Microsoft.AspNet.Identity.EntityFramework;

namespace InvoiceManager.Data
{
    public class InvoiceManagerContext : IdentityDbContext<ApplicationUser>
    {
        public InvoiceManagerContext()
            : base("InvoiceManager", throwIfV1Schema: false)
        {
        }
 
        public DbSet<Invoice> Invoices { get; set; }

        public DbSet<BankPaymentMethod> BankPaymentMethods { get; set; }

        public DbSet<BussinessCredentials> BussinessCredentialses { get; set; }

        public DbSet<InvoiceValue> InvoiceValues { get; set; }

        public DbSet<Recipient> Recipients { get; set; }

        public static InvoiceManagerContext Create()
        {
            return new InvoiceManagerContext();
        }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Invoice>().HasOptional(invoice => invoice.Recipient);

        //    base.OnModelCreating(modelBuilder);
        //}
    }
}