﻿namespace InvoiceManager.Data
{
    using Interfaces;
    using Models.EntityModels;
    using Models.EntityModels.Invoice;

    public class UnitOfWork : IUnitOfWork
    {
        private InvoiceManagerContext context;
        private IRepository<ApplicationUser> users;
        private IRepository<BankPaymentMethod> bankPaymentMethods;
        private IRepository<BussinessCredentials> bussinessCredentials;
        private IRepository<Invoice> invoices;
        private IRepository<InvoiceValue> invoiceValues;
        private IRepository<Recipient> recepients;

        public UnitOfWork()
        {
            this.context = new InvoiceManagerContext();
        }
        
    
        
        public IRepository<ApplicationUser> Users => this.users ?? (this.users = new Repository<ApplicationUser>(this.context.Users));

        public IRepository<BankPaymentMethod> BankPaymentMethods
            =>
            this.bankPaymentMethods ??
            (this.bankPaymentMethods = new Repository<BankPaymentMethod>(this.context.BankPaymentMethods));

        public IRepository<BussinessCredentials> BussinessCredentials
            =>
            this.bussinessCredentials ??
            (this.bussinessCredentials = new Repository<BussinessCredentials>(this.context.BussinessCredentialses));

        public IRepository<Invoice> Invoices
            => this.invoices ?? (this.invoices = new Repository<Invoice>(this.context.Invoices));

        public IRepository<InvoiceValue> InvoiceValues
            => this.invoiceValues ?? (this.invoiceValues = new Repository<InvoiceValue>(this.context.InvoiceValues));

        public IRepository<Recipient> Recipients
            => this.recepients ?? (this.recepients = new Repository<Recipient>(this.context.Recipients));


        public static UnitOfWork Create()
        {
            return new UnitOfWork();
        }

        public int Commit()
        {
           return context.SaveChanges();
        }

        public void Dispose()
        {
            this.context.Dispose();
        }
    }
}
