﻿namespace InvoiceManager.Data
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Linq.Expressions;
    using Interfaces;

    class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private IDbSet<TEntity> set;
      

        public Repository(IDbSet<TEntity> givenSet )
        {
            this.set = givenSet;
        }

        public void Add(TEntity entity)
        {
            this.set.Add(entity);
        }

        public void Remove(TEntity entity)
        {
            this.set.Remove(entity);
        }

        public void RemoveRange(IList<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                this.set.Remove(entity);
            }
        }

        public TEntity Find(int id)
        {
            return this.set.Find(id);
        }

        public TEntity First(Expression<Func<TEntity, bool>> expression)
        {
            return this.set.First(expression);
        }

        public TEntity FirstOrDefaultEntity(Expression<Func<TEntity, bool>> expression)
        {
            return this.set.FirstOrDefault(expression);
        }
       
        public bool Any(Expression<Func<TEntity, bool>> expression)
        {
           return this.set.Any(expression);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return this.set.ToList();
        }

        public IEnumerable<TEntity> Filter(Expression<Func<TEntity, bool>> expression)
        {
            return this.set.Where(expression);
        }

        public IEnumerable<TEntity> OrderBy(Expression<Func<TEntity, bool>> expression)
        {
           return this.set.OrderBy(expression);
        }

        //public IEnumerable<TEntity> ThenByDecending(Expression<Func<TEntity, bool>> expression)
        //{
        //    return this.set.ThenByDescending(expression);          
        //}

        public int Count()
        {
            return this.set.Count();
        }

        public int Count(Expression<Func<TEntity, bool>> expression)
        {
            return this.set.Count(expression);
        }
    }
}
