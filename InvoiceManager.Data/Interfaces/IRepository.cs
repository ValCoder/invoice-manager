﻿namespace InvoiceManager.Data.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    public interface IRepository<TEntity> where TEntity : class
    {
        void Add(TEntity entity);

        void Remove(TEntity entity);

        void RemoveRange(IList<TEntity> entities);

        TEntity Find(int id);

        TEntity FirstOrDefaultEntity(Expression<Func<TEntity, bool>> expression);

      //  ApplicationUser FirstOrDefaultUser(Expression<Func<ApplicationUser, bool>> expression);
        
        TEntity First(Expression<Func<TEntity, bool>> expression);

        bool Any(Expression<Func<TEntity, bool>> expression);

        IEnumerable<TEntity> GetAll();

        IEnumerable<TEntity> Filter(Expression<Func<TEntity, bool>> expression);

        IEnumerable<TEntity> OrderBy(Expression<Func<TEntity, bool>> expression);

      //  IEnumerable<TEntity> ThenByDecending(Expression<Func<TEntity, bool>> expression);

        int Count();

        int Count(Expression<Func<TEntity, bool>> expression);
    }
}
