﻿using System;
using System.Data.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace InvoiceManager.Data.Interfaces
{
    using Models.EntityModels;
    using Models.EntityModels.Invoice;
    public interface IUnitOfWork : IDisposable
    {
        IRepository<ApplicationUser> Users { get; }

        IRepository<BankPaymentMethod> BankPaymentMethods { get; }

        IRepository<BussinessCredentials> BussinessCredentials { get; }

        IRepository<Invoice> Invoices { get; }

        IRepository<InvoiceValue> InvoiceValues { get; }

        IRepository<Recipient> Recipients { get; }

        

    }
}
