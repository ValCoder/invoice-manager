using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace InvoiceManager.Data.Migrations
{
    
    using System.Data.Entity.Migrations;
    using System.Linq;

    public class Configuration : DbMigrationsConfiguration<InvoiceManagerContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }
        
        protected override void Seed(InvoiceManager.Data.InvoiceManagerContext context)
        {
            if (!context.Roles.Any(role => role.Name == "Basic"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole("Basic");
                manager.Create(role);
            }

            if (!context.Roles.Any(role => role.Name == "Premium"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole("Premium");
                manager.Create(role);
            }

            if (!context.Roles.Any(role => role.Name == "Medium"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole("Medium");
                manager.Create(role);
            }
        }
    }
}
