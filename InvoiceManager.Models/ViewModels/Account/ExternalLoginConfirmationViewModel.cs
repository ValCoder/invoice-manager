﻿using System.ComponentModel;
namespace InvoiceManager.Models.ViewModels.Account
{
    using System.ComponentModel.DataAnnotations;
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [DisplayName("Имейл")]
        public string Email { get; set; }
    }
}
