﻿namespace InvoiceManager.Models.ViewModels.Account
{
   
    using System.ComponentModel.DataAnnotations;

    public class LoginViewModel
    {
        [Required(ErrorMessageResourceType = typeof(Model_Languages.Resources.Login_Model.LoginModelTexts),ErrorMessageResourceName = "RequiredEmailError")]
        [Display(Name = "Email", ResourceType = typeof(Model_Languages.Resources.Login_Model.LoginModelTexts))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(Model_Languages.Resources.Login_Model.LoginModelTexts),ErrorMessageResourceName = "RequiredPasswordError")]
        [DataType(DataType.Password)]
        [Display(Name = "Password",ResourceType = typeof(Model_Languages.Resources.Login_Model.LoginModelTexts))]
        public string Password { get; set; }

        [Display(Name = "RememberMe", ResourceType = typeof(Model_Languages.Resources.Login_Model.LoginModelTexts))]
        public bool RememberMe { get; set; }
    }
}
