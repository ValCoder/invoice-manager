﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace InvoiceManager.Models.ViewModels.Account
{
    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [DisplayName("Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [DisplayName("Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }
}
