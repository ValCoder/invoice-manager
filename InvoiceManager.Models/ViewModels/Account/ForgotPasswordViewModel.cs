﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace InvoiceManager.Models.ViewModels.Account
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [DisplayName("Email")]
        public string Email { get; set; }
    }
}
