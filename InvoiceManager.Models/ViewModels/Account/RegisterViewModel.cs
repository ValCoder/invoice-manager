﻿namespace InvoiceManager.Models.ViewModels.Account
{
    using System.ComponentModel.DataAnnotations;

    public class RegisterViewModel
    {  
        [Required(ErrorMessageResourceType = typeof(Model_Languages.Resources.Register_Model.RegisterModel),ErrorMessageResourceName = "EmailRequired")]
        [Display(Name = "Email",ResourceType = typeof(Model_Languages.Resources.Register_Model.RegisterModel))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(Model_Languages.Resources.Register_Model.RegisterModel),ErrorMessageResourceName = "PassRequred")] 
        [StringLength(100,ErrorMessageResourceType = typeof(Model_Languages.Resources.Register_Model.RegisterModel),ErrorMessageResourceName = "PassLenError", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password",ResourceType = typeof(Model_Languages.Resources.Register_Model.RegisterModel))]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "ConfirmPassword",ResourceType = typeof(Model_Languages.Resources.Register_Model.RegisterModel))]
        [Compare("Password",ErrorMessageResourceType = typeof(Model_Languages.Resources.Register_Model.RegisterModel),ErrorMessageResourceName = "passwordCompareError")]
        public string ConfirmPassword { get; set; }
    }

  
}
