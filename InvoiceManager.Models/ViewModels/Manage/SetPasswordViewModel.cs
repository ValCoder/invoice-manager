﻿using System.ComponentModel.DataAnnotations;

namespace InvoiceManager.Models.ViewModels.Manage
{
    public class SetPasswordViewModel
    {
        [Required]
        [StringLength(100, ErrorMessage = "{0} трявба да е {2} символа.", MinimumLength = 6)]
        [DataType(DataType.Password, ErrorMessage = "Паролата трябва да съдържа поне един символ или число. Паролата трябва да съдържа поне една малка буква и една главна буква")]
        [Display(Name = "Нова парола")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password,ErrorMessage = "Паролата трябва да съдържа поне един символ или число. Паролата трябва да съдържа поне една малка буква и една главна буква")]
        [Display(Name = "Потвърдете новата парола")]
        [Compare("NewPassword", ErrorMessage = "Новата парола и паролата за потвърждаване не съвпадат.")]
        public string ConfirmPassword { get; set; }
    }
}