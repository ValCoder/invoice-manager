﻿using System.ComponentModel.DataAnnotations;

namespace InvoiceManager.Models.ViewModels.Manage
{
    public class AddPhoneNumberViewModel
    {
        [Required]
        [Phone]
        [Display(Name = "Phone Number")]
        public string Number { get; set; }
    }
}