﻿namespace InvoiceManager.Models.ViewModels.Invoice
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class AllInvoicesViewModel
    {
        public int Id { get; set; }

        [Display(Name = "NumberOfInvoice", ResourceType =  typeof(Model_Languages.Resources.All_Invoices_Model.AllInvoicesModelTexts))]
        public int NumberOfInvoice { get; set; }
        
        public int NumberOfAllUserInvoices { get; set; }

        [Display(Name = "TypeIOfService", ResourceType = typeof(Model_Languages.Resources.All_Invoices_Model.AllInvoicesModelTexts))]
        public string TypeOfService { get; set; }

        [Display(Name = "DateOfDelivery", ResourceType = typeof(Model_Languages.Resources.All_Invoices_Model.AllInvoicesModelTexts))]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime DateOfdDelivery { get; set; }

        [Display(Name = "ProviderName", ResourceType = typeof(Model_Languages.Resources.All_Invoices_Model.AllInvoicesModelTexts))]
        public string ProviderName { get; set; }

        [Display(Name = "RecipientName", ResourceType = typeof(Model_Languages.Resources.All_Invoices_Model.AllInvoicesModelTexts))]
        public string RecepientName { get; set; }

    }
}
