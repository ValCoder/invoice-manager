﻿namespace InvoiceManager.Models.ViewModels.Invoice
{
    public class CompanyCredentialDataViewModel
    {
       // public string TypeOfManagment { get; set; }

        public string ProviderName { get; set; }

        public string Uic { get; set; }

        public string AccountablePerson { get; set; } 

        public string Address { get; set; }
    }
}
