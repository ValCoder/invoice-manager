﻿namespace InvoiceManager.Models.ViewModels.Invoice
{
    using EntityModels.Invoice;

    public class ReadyInvoiceViewModel
    {
        public int Id { get; set; }

        public int InvoiceId { get; set; }

        public decimal AllResultValuesSum { get; set; }

        public decimal PaymentResultSum { get; set; }

        public decimal VatRatePecentage { get; set; }

        public decimal VatRatesSum { get; set; }

        public Recipient Recipient { get; set; }

        public BussinessCredentials BussinessCredentials { get; set; }

        public Invoice Invoice { get; set; }

    }
}
