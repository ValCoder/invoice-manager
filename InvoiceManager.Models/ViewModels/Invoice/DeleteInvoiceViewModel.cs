﻿namespace InvoiceManager.Models.ViewModels.Invoice
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using EntityModels.Invoice;

    public class DeleteInvoiceViewModel
    {
        public int InvoiceId { get; set; }

        [Display(Name = "NumberOfInvoice",ResourceType = typeof(Model_Languages.Resources.All_Invoices_Model.AllInvoicesModelTexts))]
        public int NumberOfInvoice { get; set; }

        public ICollection<InvoiceValue> InvoiceValues { get; set; }

        [Display(Name = "ProviderName", ResourceType = typeof(Model_Languages.Resources.All_Invoices_Model.AllInvoicesModelTexts))]
        public string ProviderName { get; set; }

        [Display(Name = "RecipientName", ResourceType = typeof(Model_Languages.Resources.All_Invoices_Model.AllInvoicesModelTexts))]
        public string RecepientName { get; set; }   
        
    }
}
