﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using InvoiceManager.Models.EntityModels.Invoice;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace InvoiceManager.Models.EntityModels
{
    public class ApplicationUser : IdentityUser
    {
        private ICollection<Invoice.Invoice> _invoices;
        public ApplicationUser()
        {
            this._invoices = new HashSet<Invoice.Invoice>();
        }

        public virtual BussinessCredentials BussinessCredentials { get; set; }
        
        public virtual ICollection<Invoice.Invoice> Invoices
        {
            get { return this._invoices; }
            set { this._invoices = value; }
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}
