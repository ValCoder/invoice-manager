﻿using System.Collections.Generic;
using InvoiceManager.Models.EntityModels.Invoice;

namespace InvoiceManager.Models.EntityModels
{
    public class User
    {
        private ICollection<Invoice.Invoice> invoices;
        public User()
        {
            this.invoices = new HashSet<Invoice.Invoice>();

        }

        public virtual ApplicationUser ApplicationUser { get; set; }

        public virtual ICollection<Invoice.Invoice> Invoices
        {
            get { return this.invoices; }
            set { this.invoices = value; }
        }

        public virtual BussinessCredentials CompanyCredentials { get; set; }
    }
}
