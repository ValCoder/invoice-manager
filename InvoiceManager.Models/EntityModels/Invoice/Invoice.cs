﻿using System;
using System.Collections.Generic;

namespace InvoiceManager.Models.EntityModels.Invoice
{
    public class Invoice
    {
        public Invoice()
        {
            this.InvoiceValues = new HashSet<InvoiceValue>();
        }

        public int Id { get; set; }

        public string UserId { get; set; }

        public int NumberOfInvoice { get; set; }

        public string PhoneNumber { get; set; }

        public string Fax { get; set; }

        public string Address { get; set; }

        public string Email { get; set; }

        public string BussinessLogoUrl { get; set; }

        public string BussinessDomainAddress { get; set; }

        public virtual BussinessCredentials BussinessCredentials { get; set; }
        
        public virtual DateTime? Date { get; set; }

        public virtual ICollection<InvoiceValue> InvoiceValues  { get; set; }

        public virtual Recipient Recipient { get; set; }

    }
}
