﻿namespace InvoiceManager.Models.EntityModels.Invoice
{
    public class Recipient
    {
        public int Id { get; set; }

        public string RecipientName { get; set; }

        public string Uic { get; set; } // ЕИК

        public string AccountablePerson { get; set; } // МОЛ    

        public string Address { get; set; }
    }
}
