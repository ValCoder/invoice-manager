﻿using System.ComponentModel.DataAnnotations;

namespace InvoiceManager.Models.EntityModels.Invoice
{
    public class BussinessCredentials
    {
        [Key]
        public int Id { get; set; }
        [MinLength(2), MaxLength(10)]
        public string TypeOfManagment { get; set; }
        [MinLength(1), MaxLength(150)]
        public string ProviderName { get; set; }

        [RegularExpression(@"([0-9]{9})")]
        public string Uic { get; set; } // ЕИК

        [MinLength(2), MaxLength(100)]
        public string AccountablePerson { get; set; } // МОЛ    
        [MinLength(2), MaxLength(50)]
        public string Address { get; set; } 
    }
}
