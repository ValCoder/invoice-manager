﻿namespace InvoiceManager.Models.EntityModels.Invoice
{
    public class BankPaymentMethod
    {
        public int Id { get; set; }

        public string Iban { get; set; }

        public string Bic { get; set; }
    }
}
