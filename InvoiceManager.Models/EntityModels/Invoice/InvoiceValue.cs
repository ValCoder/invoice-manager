﻿using System;
using System.ComponentModel.DataAnnotations;

namespace InvoiceManager.Models.EntityModels.Invoice
{
    public class InvoiceValue
    {
        public int Id { get; set; }
        [MinLength(2),MaxLength(120)]
        
        public string TypeOfService { get; set; }
        
        [MinLength(2), MaxLength(120)]
        public string TypeOfMeasure { get; set; }

       
        public int Quantity { get; set; }

       
        public decimal SinglePrice { get; set; }

        
        public decimal ResultValue { get; set; }

      
        public decimal TaxBase { get; set; }

       
        public decimal TaxRatePercentage { get; set; }

        
        public decimal VatRate { get; set; }

       
        public decimal PaymentAmount { get; set; }

        
        public DateTime DateOfDelivery { get; set; }

        public virtual BankPaymentMethod MethodOfPayment { get; set; }
    }
}
