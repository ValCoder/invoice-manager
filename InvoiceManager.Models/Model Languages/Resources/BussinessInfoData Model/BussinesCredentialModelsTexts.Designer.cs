﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InvoiceManager.Models.Model_Languages.Resources.BussinessInfoData_Model {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class BussinesCredentialModelsTexts {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal BussinesCredentialModelsTexts() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("InvoiceManager.Models.Model_Languages.Resources.BussinessInfoData_Model.BussinesC" +
                            "redentialModelsTexts", typeof(BussinesCredentialModelsTexts).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accountable person.
        /// </summary>
        public static string AccountablePerson {
            get {
                return ResourceManager.GetString("AccountablePerson", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Address registration.
        /// </summary>
        public static string AddressRegistration {
            get {
                return ResourceManager.GetString("AddressRegistration", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Comapny name.
        /// </summary>
        public static string CompanyName {
            get {
                return ResourceManager.GetString("CompanyName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The field  &quot;{0}&quot; is mandatory!.
        /// </summary>
        public static string TheFieldIsMandatory {
            get {
                return ResourceManager.GetString("TheFieldIsMandatory", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The field  &quot;{0}&quot; must be min &quot;{1}&quot; symbols long!.
        /// </summary>
        public static string TheFieldMustBeMin {
            get {
                return ResourceManager.GetString("TheFieldMustBeMin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The field  &quot;{0}&quot; must be max&quot;9&quot; symbols long!.
        /// </summary>
        public static string TheFielsMusBeMax {
            get {
                return ResourceManager.GetString("TheFielsMusBeMax", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type of management.
        /// </summary>
        public static string TypeOfManagment {
            get {
                return ResourceManager.GetString("TypeOfManagment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to UIC.
        /// </summary>
        public static string UIC {
            get {
                return ResourceManager.GetString("UIC", resourceCulture);
            }
        }
    }
}
