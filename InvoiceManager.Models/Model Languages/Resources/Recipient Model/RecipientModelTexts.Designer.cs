﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace InvoiceManager.Models.Model_Languages.Resources.Recipient_Model {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class RecipientModelTexts {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal RecipientModelTexts() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("InvoiceManager.Models.Model_Languages.Resources.Recipient_Model.RecipientModelTex" +
                            "ts", typeof(RecipientModelTexts).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Accountable person.
        /// </summary>
        public static string AccountablePerson {
            get {
                return ResourceManager.GetString("AccountablePerson", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Addres.
        /// </summary>
        public static string Address {
            get {
                return ResourceManager.GetString("Address", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The field {0} must contain max{1} symbols!.
        /// </summary>
        public static string MaxLenError {
            get {
                return ResourceManager.GetString("MaxLenError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The field {0} must contain min {1} symbols!.
        /// </summary>
        public static string MinLenError {
            get {
                return ResourceManager.GetString("MinLenError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Recipient.
        /// </summary>
        public static string RecipientName {
            get {
                return ResourceManager.GetString("RecipientName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The field &quot;{0}&quot; is mandatory!.
        /// </summary>
        public static string TheFieldIsRequiredError {
            get {
                return ResourceManager.GetString("TheFieldIsRequiredError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Uic.
        /// </summary>
        public static string Uic {
            get {
                return ResourceManager.GetString("Uic", resourceCulture);
            }
        }
    }
}
