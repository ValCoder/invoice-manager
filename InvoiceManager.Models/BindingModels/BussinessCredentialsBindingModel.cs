﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace InvoiceManager.Models.BindingModels
{
    public class BussinessCredentialsBindingModel
    {
        public int Id { get; set; }

        [DisplayName("Вид на управление")]
        [MinLength(2, ErrorMessage = "Името трябва да е най-малко с дължина два символа!"), MaxLength(10, ErrorMessage = "Името е по-дълго от десет символа!")]
        public string TypeOfManagment { get; set; }

        [DisplayName("Доставчик")]
        [MinLength(1), MaxLength(150)]
        public string ProviderName { get; set; }

        [DisplayName("ЕИК")]
        [RegularExpression(@"([0-9]{9})", ErrorMessage = "ЕИК трябва да съдържа девет цифри")]
        public string Uic { get; set; } // ЕИК

        [DisplayName("МОЛ")]
        [MinLength(2, ErrorMessage = "Името трявба да бъде най-малко два символа!"), MaxLength(100, ErrorMessage = "Името трябва да бъде по-малко от сто символа!")]
        public string AccountablePerson { get; set; } // МОЛ    

        [DisplayName("Адрес")]
        [MinLength(2, ErrorMessage = "Адресът трябва да съдържа най-малко два символа"), MaxLength(50, ErrorMessage = "Адресът трябва да съдържа по-малко от сто символа!")]
        public string Address { get; set; }
    }
}
