﻿namespace InvoiceManager.Models.BindingModels.Recipient
{
    using System.ComponentModel.DataAnnotations;

    public class RecipientBindingModel
    {
        #region Attributres for RecipientName property
        [Required(ErrorMessageResourceName = "TheFieldIsRequiredError",ErrorMessageResourceType = typeof(Model_Languages.Resources.Recipient_Model.RecipientModelTexts))]
        [Display(Name = "RecipientName", ResourceType = typeof(Model_Languages.Resources.Recipient_Model.RecipientModelTexts))]
        [MinLength(1,ErrorMessageResourceName = "MinLenError",ErrorMessageResourceType = typeof(Model_Languages.Resources.Recipient_Model.RecipientModelTexts)), MaxLength(150, ErrorMessageResourceName = "MaxLenError",ErrorMessageResourceType = typeof(Model_Languages.Resources.Recipient_Model.RecipientModelTexts))]
        #endregion
        public string RecipientName { get; set; }


        #region Attributes for Uic property
        [Required(ErrorMessageResourceName = "TheFieldIsRequiredError", ErrorMessageResourceType = typeof(Model_Languages.Resources.Recipient_Model.RecipientModelTexts))]
        //TODO: UIC validation logic(only numbers or numbers with lethers !!!)
        [Display(Name = "Uic",ResourceType = typeof(Model_Languages.Resources.Recipient_Model.RecipientModelTexts))]
        #endregion
        public string Uic { get; set; }


        #region Attributes for AccountablePerson property
        [Required(ErrorMessageResourceName = "TheFieldIsRequiredError", ErrorMessageResourceType = typeof(Model_Languages.Resources.Recipient_Model.RecipientModelTexts))]
        [Display(Name = "AccountablePerson",ResourceType = typeof(Model_Languages.Resources.Recipient_Model.RecipientModelTexts))]
        [MinLength(2, ErrorMessageResourceName = "MinLenError", ErrorMessageResourceType = typeof(Model_Languages.Resources.Recipient_Model.RecipientModelTexts)), MaxLength(100, ErrorMessageResourceName = "MaxLenError", ErrorMessageResourceType = typeof(Model_Languages.Resources.Recipient_Model.RecipientModelTexts))]
        #endregion
        public string AccountablePerson { get; set; }


        #region Attributes for Address property
        [Required(ErrorMessageResourceName = "TheFieldIsRequiredError", ErrorMessageResourceType = typeof(Model_Languages.Resources.Recipient_Model.RecipientModelTexts))]
        [Display(Name = "Address", ResourceType = typeof(Model_Languages.Resources.Recipient_Model.RecipientModelTexts))]
        [MinLength(2, ErrorMessageResourceName = "MinLenError", ErrorMessageResourceType = typeof(Model_Languages.Resources.Recipient_Model.RecipientModelTexts)), MaxLength(150, ErrorMessageResourceName = "MaxLenError", ErrorMessageResourceType = typeof(Model_Languages.Resources.Recipient_Model.RecipientModelTexts))]
        #endregion
        public string Address { get; set; }
    }
}
