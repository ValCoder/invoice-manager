﻿namespace InvoiceManager.Models.BindingModels.Invoice
{
    using System.ComponentModel.DataAnnotations;
    using EntityModels.Invoice;

    public class AddInvoiceInfoBindingModel
    {
        #region Attributes for NumberOfInvoice property
        [Display(Name = "StartingInvoiceNumber",ResourceType = typeof(Model_Languages.Resources.Invoice_Info_Model.InvoiceInfoModelTexts))]
        [Required(ErrorMessageResourceName = "TheFieldIsRequeiredError",ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Info_Model.InvoiceInfoModelTexts))]
        #endregion

        public int NumberOfInvoice { get; set; }

        #region Attributes for PhoneNumber property
        [Display(Name = "ComapnyPhoneNumber",ResourceType = typeof(Model_Languages.Resources.Invoice_Info_Model.InvoiceInfoModelTexts))]
        [Required(ErrorMessageResourceName = "TheFieldIsRequeiredError",ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Info_Model.InvoiceInfoModelTexts))]
        #endregion

        public string PhoneNumber { get; set; }

        #region Attributes for Fax property
        [Display(Name = "CompanyFax",ResourceType = typeof(Model_Languages.Resources.Invoice_Info_Model.InvoiceInfoModelTexts))]
        #endregion

        public string Fax { get; set; }

        #region Attributes for Email property
        [Display(Name = "CompanyEmail",ResourceType = typeof(Model_Languages.Resources.Invoice_Info_Model.InvoiceInfoModelTexts))]
        [Required(ErrorMessageResourceName = "TheFieldIsRequeiredError",ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Info_Model.InvoiceInfoModelTexts))]
        #endregion

        public string Email { get; set; }


        public string BussinessLogoUrl { get; set; }


        #region Attributes for BussinessDomainAddress property
        [Display(Name = "CompanyDomain", ResourceType = typeof(Model_Languages.Resources.Invoice_Info_Model.InvoiceInfoModelTexts))]
        #endregion

        public string BussinessDomainAddress { get; set; }


        public virtual BussinessCredentials BussinessCredentials { get; set; }
        
    }
}
