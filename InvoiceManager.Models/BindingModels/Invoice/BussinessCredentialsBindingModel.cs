﻿namespace InvoiceManager.Models.BindingModels.Invoice
{
    using System.ComponentModel.DataAnnotations;

    public class BussinessCredentialsBindingModel
    {

        #region Attributes for Type of managment property

        

       [Required(ErrorMessageResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts), ErrorMessageResourceName = "TheFieldIsMandatory")]
        [Display(Name = "TypeOfManagment", ResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts))]
        [MinLength(2,ErrorMessageResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts),ErrorMessageResourceName = "TheFieldMustBeMin"), MaxLength(10,ErrorMessageResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts),ErrorMessageResourceName = "TheFielsMusBeMax")]
       #endregion
        public string TypeOfManagment { get; set; }

        #region Attributes for Provider name property
        [Required(ErrorMessageResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts), ErrorMessageResourceName = "TheFieldIsMandatory")]
        [Display(Name = "CompanyName", ResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts))]
        [MinLength(1, ErrorMessageResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts), ErrorMessageResourceName = "TheFieldMustBeMin"), MaxLength(150, ErrorMessageResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts), ErrorMessageResourceName = "TheFielsMusBeMax")]
        #endregion
        public string ProviderName { get; set; }

        #region Attributes for Uic property
        [Required(ErrorMessageResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts), ErrorMessageResourceName = "TheFieldIsMandatory")]
        [Display(Name = "UIC", ResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts))]
        [RegularExpression(@"([0-9]{9})", ErrorMessageResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts),ErrorMessageResourceName = "TheFielsMusBeMax")]
        #endregion
        public string Uic { get; set; }

        #region Attributes for Acountable person property
        [Required(ErrorMessageResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts), ErrorMessageResourceName = "TheFieldIsMandatory")]
        [Display(Name = "AccountablePerson",ResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts))]
        [MinLength(2, ErrorMessageResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts), ErrorMessageResourceName = "TheFieldMustBeMin"), MaxLength(100, ErrorMessageResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts), ErrorMessageResourceName = "TheFielsMusBeMax")]
        #endregion
        public string AccountablePerson { get; set; }

        #region Attributes for Address property
        [Required(ErrorMessageResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts), ErrorMessageResourceName = "TheFieldIsMandatory")]
        [Display(Name =  "AddressRegistration",ResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts))]
        [MinLength(1, ErrorMessageResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts), ErrorMessageResourceName = "TheFieldMustBeMin"), MaxLength(50, ErrorMessageResourceType = typeof(Model_Languages.Resources.BussinessInfoData_Model.BussinesCredentialModelsTexts), ErrorMessageResourceName = "TheFielsMusBeMax")]
        #endregion
        public string Address { get; set; }
    }
}
