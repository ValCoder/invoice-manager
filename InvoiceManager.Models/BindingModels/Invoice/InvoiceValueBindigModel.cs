﻿namespace InvoiceManager.Models.BindingModels.Invoice
{
    using System;
    using System.ComponentModel.DataAnnotations;

    public class InvoiceValueBindigModel
    {
        #region Attributes for TypeOfMeasure property

        [Required(ErrorMessageResourceName = "TheFieldIsRequeredError",ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        [Display(Name = "TypeOfService",ResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        [MinLength(2,ErrorMessageResourceName = "MinLenError",ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts)), MaxLength(120,ErrorMessageResourceName = "MaxLenErro",ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        #endregion
        public string TypeOfService { get; set; }

        #region Attributes for TypeOfMeasure property

        [Required(ErrorMessageResourceName = "TheFieldIsRequeredError", ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        [Display(Name = "TypeOfMeasure",ResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        [MinLength(2, ErrorMessageResourceName = "MinLenError", ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts)), MaxLength(120, ErrorMessageResourceName = "MaxLenErro", ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        #endregion
        public string TypeOfMeasure { get; set; }


        #region Attributes for Quantity property

        [Required(ErrorMessageResourceName = "TheFieldIsRequeredError", ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        [Display(Name = "Quantity",ResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        [Range(0, int.MaxValue,ErrorMessageResourceName = "QuantityRangeError",ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
       // [RegularExpression(@"^[0-9]*$", ErrorMessage = "Полето трябва да съдържа само цели числа")]
        #endregion
        public int Quantity { get; set; }

        #region Attributes for SinglePrice property

        [Required(ErrorMessageResourceName = "TheFieldIsRequeredError", ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        [Display(Name = "SinglePrice",ResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        [RegularExpression(@"^[0-9]\d*(\.\d+)?$",ErrorMessageResourceName = "OnlyDigitsError",ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        #endregion
        public decimal SinglePrice { get; set; }


        #region Attributes for ResultValue property
        [Required(ErrorMessageResourceName = "TheFieldIsRequeredError",ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        [Display(Name =  "ResultValue",ResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        #endregion
        public decimal ResultValue { get; set; }


        #region Attributes for TaxBase property
        [Required(ErrorMessageResourceName = "TheFieldIsRequeredError",ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        [Display(Name = "TaxBase",ResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        #endregion
        public decimal TaxBase { get; set; }


        #region Attributes for TaxRatePercentage
        [Required(ErrorMessageResourceName = "TheFieldIsRequeredError",ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        [Display(Name = "TaxRatePercentage",ResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        [RegularExpression(@"^[0-9]\d*(\.\d+)?$", ErrorMessageResourceName = "OnlyDigitsError", ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        #endregion
        public decimal TaxRatePercentage { get; set; }


        #region Attributes for VatRate property
        [Required(ErrorMessageResourceName = "TheFieldIsRequeredError", ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        [Display(Name = "VatRate",ResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        #endregion
        public decimal VatRate { get; set; }


        #region Attributes for PaymentAmount property
        [Required(ErrorMessageResourceName = "TheFieldIsRequeredError", ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        [Display(Name = "PaymnetAmount",ResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        #endregion
        public decimal PaymentAmount { get; set; }


        #region Attributes for DateOfDelivery property
        [Required(ErrorMessageResourceName = "TheFieldIsRequeredError", ErrorMessageResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        [Display(Name = "DateOfDelivery",ResourceType = typeof(Model_Languages.Resources.Invoice_Value_Model.InvoiceValueModelTexts))]
        #endregion
        public DateTime DateOfDelivery { get; set; }



        //// [Required(ErrorMessage = "Полето е задължително")]
        //[DisplayName("Метод на плащане")]
        //public virtual BankPaymentMethod MethodOfPayment { get; set; }
        
    }
}
