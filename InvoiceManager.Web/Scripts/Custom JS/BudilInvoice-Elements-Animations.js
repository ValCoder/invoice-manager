﻿// Toggle Buttons

toggleViewReadyInvoice = function () {

    $('#view-invoice-button').removeAttr('hidden');
}

toogleDownloadButton = function () {

    $('#download-invoice').removeAttr('hidden');
}


// Toggle animations 

toogleAnimation = function () {
    $('#succsess-animation').removeAttr('hidden');
    $('#invoiceValueForm').removeAttr('hidden');
}

toogleAnimation1 = function () {
    $('#succsess-animation1').removeAttr('hidden');
}


// Hide Elements 

hideJumbotronInfoAndRecipientButton = function () {

    $('#jumbotron-info').hide();
    $('#pulse-button-recipient').hide();
}

hideCompanyInfoButton = function () {

    $('#company-data-button').hide();
}


// Reset Invoice Value Form

resetForm = function () {
    document.getElementById("invoiceValueForm").reset();
    document.getElementById("succsess-animation1").setAttribute("hidden", "hidden");


    // fade out animation
}