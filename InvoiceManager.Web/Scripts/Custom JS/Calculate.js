﻿CalculateQuantityAndSinglePrice = function () {
    var singlePrice = parseFloat($('#single-price').val());

    var qunatity = parseFloat($('#quantity').val());

    var taxRatePercentage = parseInt($('#tax-rate-percentage').val());

    var resultValue = qunatity * singlePrice;

    var vatResult = resultValue * (taxRatePercentage / 100);

    var resultWithVat = vatResult + resultValue;

    $('#result-value').val(resultValue);

    $('#tax-base').val(resultValue);

    $('#vat-rate').val(vatResult);

    $('#payment-amount').val(resultWithVat);
}