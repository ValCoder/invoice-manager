﻿var i = 1;
$("#addRow").click(function () {
    $("tbody tr:first").clone().find('td').each(function () {
        $(this).val('').attr('id', function (_, id) { return id + i });
    }).end().appendTo("table");
    i++;
}); 