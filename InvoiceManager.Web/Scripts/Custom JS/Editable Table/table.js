﻿$('#editable-table').Tabledit({
    url: '',
    deleteButton: false,
    saveButton: true,
    autoFocus: true,
    hideIdentifier: true,
    
    buttons: {
        edit: {
            class: 'btn btn-sm btn-primary',
            html: '<span class="glyphicon glyphicon-pencil"></span> &nbsp EDIT',
            action: 'edit'
        }
    },
    columns: {
        identifier: [0, 'id'],
        editable: [[1, 'TypeOfService'], [2, 'TypeOfMeasure'], [3, 'Quantity'], [4, 'SinglePrice']]
    }
});



