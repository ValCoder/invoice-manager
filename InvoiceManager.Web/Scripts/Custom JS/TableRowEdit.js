﻿$('#editable-table').Tabledit({
    
    deleteButton: true,
    saveButton: true,
    autoFocus: false,
    buttons: {
        edit: {
            class: 'btn btn-sm btn-primary',
            html: '<span class="glyphicon glyphicon-pencil"></span> &nbsp EDIT',
            action: 'edit'
        },
        delete: {
            class: 'btn btn-sm btn-danger',
            html: '<span class="glyphicon glyphicon-trash"></span> &nbsp DELETE',
            action: 'delete'
        },
        confirm: {
            class: 'btn btn-sm btn-default',
            html: 'Are you sure?'
        }
    },
    columns: {
        identifier: [0, 'id'],
        editable: [[0, 'Вид Услуга'], [1, 'Мярка'], [2, 'Количество'], [3, 'Единична цена'], [4, 'Стойност']]
    }
});