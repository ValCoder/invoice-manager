﻿var i = 1;
$("button").click(function () {
    $("tbody tr:first").clone().find("input").each(function () {
        $(this).val('').attr('id', function (_, id) { return id + i });
    }).end().appendTo("table");
    i++;
});