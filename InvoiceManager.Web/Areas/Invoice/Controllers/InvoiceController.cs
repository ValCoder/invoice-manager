﻿namespace InvoiceManager.Web.Areas.Invoice.Controllers
{
    using System.Collections.Generic;
    using System.Net;
    using System.Web.Mvc;
    using System.Web.Security;
    using Models.BindingModels.Invoice;
    using Models.BindingModels.Recipient;
    using Models.ViewModels.Invoice;
    using Services;
    using Microsoft.AspNet.Identity;
    using Rotativa;
    using Rotativa.Options;

    [Authorize]
    [RoutePrefix("invoice")]
   // [Authorize(Roles = "Basic")]
    public class InvoiceController : Controller
    {
        private readonly InvoiceControllerService _service;
        public InvoiceController()
        {
            this._service = new InvoiceControllerService();
        }

        [HttpGet]
        [Route("home")]
        public ActionResult Home()
        {
            return View();
        }

        [HttpGet]
        [Route("add/bussinessinfo")]
        public ActionResult AddBussinessInfo()
        {
            var currentUserId = this.User.Identity.GetUserId();

            if (!this._service.IsFirstTimeToCreateInvoice(currentUserId))
            {
                return this.View();
            }
            return RedirectToAction("CreateNewInvoice");
        }

        //OK
        [HttpPost]
        [Route("add/bussinessinfo")]
        public ActionResult AddBussinessInfo([Bind(Include = "TypeOfManagment,ProviderName,Uic,AccountablePerson,Address")]BussinessCredentialsBindingModel bcbm)
        {
            if (ModelState.IsValid)
            {
                var currentUserId = this.User.Identity.GetUserId();

                this._service.AddBussinessCredentialsToCurrentUser(currentUserId, bcbm);

                return RedirectToAction("AddInvoiceInfo");
            }

            return RedirectToAction("Home");
        }

        [HttpGet]
        [Route("add/invoiceinfo")]
        public ActionResult AddInvoiceInfo()
        {
            var currentUserId = this.User.Identity.GetUserId();

            if (!this._service.IsFirstTimeToCreateInvoice(currentUserId))
            {
                return this.View();
            }
            return RedirectToAction("CreateNewInvoice");

        }

        [HttpPost]
        [Route("add/invoiceinfo")]
        public ActionResult AddInvoiceInfo([Bind(Include = "NumberOfInvoice,PhoneNumber,Fax,Address,Email,BussinessLogo,BussinessDomainAddress,BussinessCredentials")]AddInvoiceInfoBindingModel aiibm)
        {
            if (ModelState.IsValid)
            {
                var currentUserId = this.User.Identity.GetUserId();
                this._service.AddInvoiceInfo(currentUserId, aiibm);
                return RedirectToAction("BuildInvoice");
            }
            return this.PartialView("Error");
        }

        [HttpGet]
        [Route("new")]
        public ActionResult CreateNewInvoice()
        {
            var currentUserId = this.User.Identity.GetUserId();

            // chek if user has invoice info in db, if false create new one
            if (!this._service.IsFirstTimeToCreateInvoice(currentUserId))
            {
                return RedirectToAction("AddBussinessInfo");
            }



            // else create new invoice for current user with next number in NumberOfInvoice

            this._service.CreateNewInvoiceForCurrentUser(currentUserId);
            return this.RedirectToAction("BuildInvoice");
        }

        [HttpGet]
        [Route("build")]
        public ActionResult BuildInvoice()
        {
            return this.View();
        }

        [HttpPost]
        [Route("add/value")]
        public ActionResult AddInvoiceValue([Bind(Include = "TypeOfService,TypeOfMeasure,Quantity,SinglePrice,ResultValue,TaxBase,TaxRatePercentage,VatRate,PaymentAmount,DateOfDelivery")]InvoiceValueBindigModel ivbm)
        {
            if (ModelState.IsValid)
            {
                var currentUserId = this.User.Identity.GetUserId();

                var hasAddedValue = this._service.AddInvoiceValue(currentUserId, ivbm);

                if (hasAddedValue)
                {
                    return this.View("BuildInvoice");
                }
            }
            return this.PartialView("Error");
        }

        [HttpGet]
        [OutputCache(Duration = 10)]
        [Route("companycd")]
        //[ChildActionOnly]
        public ActionResult GetCompanyCredentialsData()
        {
            var currentUserId = this.User.Identity.GetUserId();

            var companyCredentialsData = this._service.GetBussinesCredentialForCurrentUser(currentUserId);

            return this.PartialView("PartialViews/_CompanyCredentialData", companyCredentialsData);
        }


        [HttpGet]
        [Route("recepient")]
        public ActionResult AddRecepient()
        {
            return this.PartialView("PartialViews/_AddRecipient");
        }

        [HttpPost]
        [Route("recepient")]
        [OutputCache(Duration = 10)]
        public ActionResult AddRecepient([Bind(Include = "RecipientName,Uic,Address,AccountablePerson")]RecipientBindingModel rbm)
        {
            var currentUserId = this.User.Identity.GetUserId();
            this._service.AddRecepient(rbm, currentUserId);
            return this.PartialView("PartialViews/_AddRecipient");
        }

        [HttpGet]
        [Route("all")]
        public ActionResult AllInvoices()
        {
            var currentUserId = this.User.Identity.GetUserId();

            IEnumerable<AllInvoicesViewModel> viewModels = this._service.GetAllInvoicesForCurrentUser(currentUserId);

            return this.View(viewModels);
        }

        [HttpGet]
        [Route("ready/{id?}")]
        public ActionResult GetReadyInvoice(int? id)
        {
            var currentUserId = this.User.Identity.GetUserId();
            var viewModel = this._service.GetReadyInvoice(currentUserId,id);
            if (viewModel != null)
            {
                return this.PartialView("PartialViews/_GetReadyInvoice", viewModel);
            }
            return this.PartialView("Error");
        }

        [HttpGet]
        [Route("pdf/{id?}")]
        [OutputCache(Duration = 5)]
        public ActionResult GetInvoiceToDownload(int? id)
        {
            var currentUserId = this.User.Identity.GetUserId();

            var readyInvoiceViewModel = this._service.GetReadyInvoice(currentUserId,id);

            //return this.View(readyInvoiceViewModel);
            
            return new ViewAsPdf()
            {
                FormsAuthenticationCookieName = FormsAuthentication.FormsCookieName,
                Model = readyInvoiceViewModel,
                ViewName = "GetInvoiceToDownload",
                FileName = $"{App_Languages.Resources.View_Invoice_Page.ViewReadyInvvoice_Texts.NumberOfInvoice} {readyInvoiceViewModel.Invoice.NumberOfInvoice}.pdf",
                IsLowQuality = false,
                IsJavaScriptDisabled = false,
                IsBackgroundDisabled = true,
                IsGrayScale = true,
                MinimumFontSize = 18,
                PageOrientation = Orientation.Portrait,
                PageSize = Size.A4,
            };
        }

        [HttpGet]
        [Route("delete/{id?}")]
        public ActionResult DeleteInvoice(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway);
            }

            var currentUserId = this.User.Identity.GetUserId();
            var invoice = this._service.GetInvoiceByIdToDelete(currentUserId, id);

            if (invoice == null)
            {
                return this.HttpNotFound();
            }

            return this.View(invoice);
        }

        [HttpPost]
        [Route("delete/{id?}")]
        public ActionResult DeleteInvoiceConfimerd(int id)
        {
            this._service.DeleteCurrentUserInvoiceById(id, this.User.Identity.GetUserId());

            return this.RedirectToAction("AllInvoices");
        }

        [HttpGet]
        [Route("details/{id?}")]
        public ActionResult DetailsInvoice(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadGateway);
            }

            var currentUserId = this.User.Identity.GetUserId();
            var invoice = this._service.GetReadyInvoice(currentUserId, id);

            if (invoice == null)
            {
                return HttpNotFound();
            }

            return this.View(invoice);
        }
    }
}