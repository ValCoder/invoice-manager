﻿using System.Web.Mvc;

namespace InvoiceManager.Web.Controllers
{
    [RoutePrefix("home")]
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [Route("about")]
        public ActionResult About()
        {
            return this.View();
        }
    }
}