﻿namespace InvoiceManager.Web.Controllers
{
    using System.Globalization;
    using System.Threading;
    using System.Web;
    using System.Web.Mvc;

    [RoutePrefix("language")]
    [AllowAnonymous]
    public class LanguageController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }


        [HttpGet]
        [Route("change")]
        public ActionResult Change(string languageAbbrevation)
        {
            if (languageAbbrevation != null)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageAbbrevation);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageAbbrevation);
            }

            HttpCookie cookie = new HttpCookie("Language");
            cookie.Value = languageAbbrevation;
            this.Response.Cookies.Add(cookie);
            
            return this.View("Index");
        }
    }
}