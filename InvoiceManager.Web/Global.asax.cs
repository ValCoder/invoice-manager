﻿using System;
using System.Web;

namespace InvoiceManager.Web
{
    using System.Data.Entity;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using AutoMapper;
    using Data.Migrations;
    using Models.BindingModels.Invoice;
    using Models.EntityModels.Invoice;

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ConfigurateMappings();
            ConfigurateMigrationStrategy();
           
        }

        protected void Application_BeginRequest(object sender, EventArgs eventArgs)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies["Language"];

            if (cookie?.Value != null)
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(cookie.Value);
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(cookie.Value);
            }

            else
            {
                System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en");
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en");
            }
        }

        private void ConfigurateMappings()
        {
           Mapper.Initialize(map =>
           {
               map.CreateMap<BussinessCredentialsBindingModel, BussinessCredentials>();

               map.CreateMap<AddInvoiceInfoBindingModel, Invoice>();
              

               map.CreateMap<InvoiceValueBindigModel, InvoiceValue>();

           });
        }

        private void ConfigurateMigrationStrategy()
        {
            var migrationStrategy = new MigrateDatabaseToLatestVersion<Data.InvoiceManagerContext, Configuration>();
            Database.SetInitializer(migrationStrategy);
        }


        
    }
}
